const changeTheme = document.querySelector('.change-theme')
const changeThemeSpan = document.querySelector('.change-theme span') 
const mainLink = document.head.querySelector('link.main-link')



changeTheme.addEventListener('click', (e) => {
  e.preventDefault()
  if (localStorage.getItem('theme') === 'dark') {
    localStorage.removeItem('theme')
  } else {
    localStorage.setItem('theme', 'dark')
  }
  addDarkTheme()
})



function addDarkTheme() {
  if (localStorage.getItem('theme')) {
    mainLink.setAttribute('href', './style/style-dark.css')

    changeThemeSpan.textContent = 'dark_mode'
  } else {
    mainLink.setAttribute('href', './style/style-light.css')

    changeThemeSpan.textContent = 'sunny'
  }
}
addDarkTheme()
